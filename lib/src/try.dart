// ignore_for_file: unnecessary_new

part of dartz;

abstract class Try<L extends Exception, R>
    implements TraversableMonadOps<Try<L, dynamic>, R> {
  const Try();


  B fold<B>(B ifFailure(L l), B ifSuccess(R r));

  Try<L, R> orElse(Try<L, R> other()) =>
      fold((_) => other(), (_) => this);

  R getOrElse(R dflt()) => fold((_) => dflt(), id);

  R operator |(R dflt) => getOrElse(() => dflt);

  Try<L2, R> failureMap<L2 extends Exception>(L2 f(L l)) => fold((L l) => failure(f(l)), success);

  Option<R> toOption() => fold((_) => none(), some);

  bool isFailure() => fold((_) => true, (_) => false);

  bool isSuccess() => fold((_) => false, (_) => true);


  @override Try<L, R2> map<R2>(R2 f(R r)) =>
      fold(failure, (R r) => success(f(r)));

  @override Try<L, R2> bind<R2>(Function1<R, Try<L, R2>> f) =>
      fold(failure, f);

  @override Try<L, R2> flatMap<R2>(Function1<R, Try<L, R2>> f) =>
      fold(failure, f);

  @override Try<L, R2> andThen<R2>(Try<L, R2> next) =>
      fold(failure, (_) => next);

  IList<Try<L, R2>> traverseIList<R2>(IList<R2> f(R r)) =>
      fold((l) => cons(failure(l), nil()), (R r) => f(r).map(success));

  IVector<Try<L, R2>> traverseIVector<R2>(IVector<R2> f(R r)) =>
      fold((l) => emptyVector<Try<L, R2>>().appendElement(failure(l)), (R r) =>
          f(r).map(success));

  Future<Try<L, R2>> traverseFuture<R2>(Future<R2> f(R r)) =>
      fold((l) => new Future.microtask(() => failure(l)), (R r) =>
          f(r).then(success));

  State<S, Try<L, R2>> traverseState<S, R2>(State<S, R2> f(R r)) =>
      fold((l) => new State((s) => tuple2(failure(l), s)), (r) => f(r).map(success));

  static IList<Try<L, R>> sequenceIList<L extends Exception, R>(Try<L, IList<R>> elr) =>
      elr.traverseIList(id);

  static IVector<Try<L, R>> sequenceIVector<L extends Exception, R>(
      Try<L, IVector<R>> evr) => evr.traverseIVector(id);

  static Future<Try<L, R>> sequenceFuture<L extends Exception, R>(Try<L, Future<R>> efr) =>
      efr.traverseFuture(id);

  static State<S, Try<L, R>> sequenceState<S, L extends Exception, R>(
      Try<L, State<S, R>> esr) => esr.traverseState(id);

  static Try<L, A> catching<L extends Exception, A>(Function0<A> thunk) {
    try {
      return success(thunk());
    } on L catch (e) {
      return failure(e);
    }
  }

  Try<L, R> filter(bool predicate(R r), L fallback()) =>
      fold((_) => this, (r) => predicate(r) ? this : failure(fallback()));

  Try<L, R> where(bool predicate(R r), L fallback()) =>
      filter(predicate, fallback);

  static Try<L, C> map2<L extends Exception, A, A2 extends A, B, B2 extends B, C>(
      Try<L, A2> fa, Try<L, B2> fb, C fun(A a, B b)) =>
      fa.fold(failure, (a) => fb.fold(failure, (b) => success(fun(a, b))));

  static Try<L,
      D> map3<L extends Exception, A, A2 extends A, B, B2 extends B, C, C2 extends C, D>(
      Try<L, A2> fa, Try<L, B2> fb, Try<L, C2> fc,
      D fun(A a, B b, C c)) =>
      fa.fold(failure, (a) =>
          fb.fold(failure, (b) => fc.fold(failure, (c) => success(fun(a, b, c)))));

  static Try<L,
      E> map4<L extends Exception, A, A2 extends A, B, B2 extends B, C, C2 extends C, D, D2 extends D, E>(
      Try<L, A2> fa, Try<L, B2> fb, Try<L, C2> fc, Try<L, D2> fd,
      E fun(A a, B b, C c, D d)) =>
      fa.fold(failure, (a) => fb.fold(failure, (b) => fc.fold(
          failure, (c) => fd.fold(failure, (d) => success(fun(a, b, c, d))))));

  static Try<L,
      F> map5<L extends Exception, A, A2 extends A, B, B2 extends B, C, C2 extends C, D, D2 extends D, E, E2 extends E, F>(
      Try<L, A2> fa, Try<L, B2> fb, Try<L, C2> fc, Try<L, D2> fd,
      Try<L, E2> fe, F fun(A a, B b, C c, D d, E e)) =>
      fa.fold(failure, (a) => fb.fold(failure, (b) => fc.fold(failure, (c) => fd.fold(
          failure, (d) => fe.fold(failure, (e) => success(fun(a, b, c, d, e)))))));

  static Try<L,
      G> map6<L extends Exception, A, A2 extends A, B, B2 extends B, C, C2 extends C, D, D2 extends D, E, E2 extends E, F, F2 extends F, G>(
      Try<L, A2> fa, Try<L, B2> fb, Try<L, C2> fc, Try<L, D2> fd,
      Try<L, E2> fe, Try<L, F2> ff,
      G fun(A a, B b, C c, D d, E e, F f)) =>
      fa.fold(failure, (a) => fb.fold(failure, (b) => fc.fold(failure, (c) => fd.fold(
          failure, (d) => fe.fold(failure, (e) => ff.fold(
          failure, (f) => success(fun(a, b, c, d, e, f))))))));

  static Try<L, C> mapM2<L extends Exception, A, A2 extends A, B, B2 extends B, C>(
      Try<L, A2> fa, Try<L, B2> fb, Try<L, C> f(A a, B b)) =>
      fa.bind((a) => fb.bind((b) => f(a, b)));

  static Function1<Try<L, A>, Try<L, B>> lift<L extends Exception, A, B>(B f(A a)) =>
      ((Try<L, A> oa) => oa.map(f));

  static Function2<Try<L, A>, Try<L, B>, Try<L, C>> lift2<L extends Exception, A, B, C>(
      C f(A a, B b)) => (Try<L, A> fa, Try<L, B> fb) => map2(fa, fb, f);

  static Function3<Try<L, A>,
      Try<L, B>,
      Try<L, C>,
      Try<L, D>> lift3<L extends Exception, A, B, C, D>(D f(A a, B b, C c)) =>
          (Try<L, A> fa, Try<L, B> fb, Try<L, C> fc) =>
          map3(fa, fb, fc, f);

  static Function4<Try<L, A>,
      Try<L, B>,
      Try<L, C>,
      Try<L, D>,
      Try<L, E>> lift4<L extends Exception, A, B, C, D, E>(E f(A a, B b, C c, D d)) =>
          (Try<L, A> fa, Try<L, B> fb, Try<L, C> fc,
          Try<L, D> fd) => map4(fa, fb, fc, fd, f);

  static Function5<Try<L, A>,
      Try<L, B>,
      Try<L, C>,
      Try<L, D>,
      Try<L, E>,
      Try<L, F>> lift5<L extends Exception, A, B, C, D, E, F>(F f(A a, B b, C c, D d, E e)) =>
          (Try<L, A> fa, Try<L, B> fb, Try<L, C> fc, Try<L, D> fd,
          Try<L, E> fe) => map5(fa, fb, fc, fd, fe, f);

  static Function6<Try<L, A>,
      Try<L, B>,
      Try<L, C>,
      Try<L, D>,
      Try<L, E>,
      Try<L, F>,
      Try<L, G>> lift6<L extends Exception, A, B, C, D, E, F, G>(
      G f(A a, B b, C c, D d, E e, F f)) =>
          (Try<L, A> fa, Try<L, B> fb, Try<L, C> fc, Try<L, D> fd,
          Try<L, E> fe, Try<L, F> ff) =>
          map6(
              fa,
              fb,
              fc,
              fd,
              fe,
              ff,
              f);

  @override String toString() => fold((l) => 'Failure($l)', (r) => 'Success($r)');

  @override B foldMap<B>(Monoid<B> bMonoid, B f(R r)) =>
      fold((_) => bMonoid.zero(), f);

  @override Try<L, B> mapWithIndex<B>(B f(int i, R r)) =>
      map((r) => f(0, r));

  @override Try<L, Tuple2<int, R>> zipWithIndex() =>
      map((r) => tuple2(0, r));

  @override bool all(bool f(R r)) => map(f) | true;

  @override bool every(bool f(R r)) => all(f);

  @override bool any(bool f(R r)) => map(f) | false;

  @override R concatenate(Monoid<R> mi) => getOrElse(mi.zero);

  @override Option<R> concatenateO(Semigroup<R> si) => toOption();

  @override B foldFailure<B>(B z, B f(B previous, R r)) =>
      fold((_) => z, (a) => f(z, a));

  @override B foldFailureWithIndex<B>(B z, B f(B previous, int i, R r)) =>
      fold((_) => z, (a) => f(z, 0, a));

  @override Option<B> foldMapO<B>(Semigroup<B> si, B f(R r)) =>
      map(f).toOption();

  @override B foldSuccess<B>(B z, B f(R r, B previous)) =>
      fold((_) => z, (a) => f(a, z));

  @override B foldSuccessWithIndex<B>(B z, B f(int i, R r, B previous)) =>
      fold((_) => z, (a) => f(0, a, z));

  @override R intercalate(Monoid<R> mi, R r) => fold((_) => mi.zero(), id);

  @override int length() => fold((_) => 0, (_) => 1);

  @override Option<R> maximum(Order<R> or) => toOption();

  @override Option<R> minimum(Order<R> or) => toOption();

  @override Try<L, B> replace<B>(B replacement) => map((_) => replacement);

  Try<L, R> reverse() => this;

  @override Try<L, Tuple2<B, R>> strengthL<B>(B b) =>
      map((a) => tuple2(b, a));

  @override Try<L, Tuple2<R, B>> strengthR<B>(B b) =>
      map((a) => tuple2(a, b));

  @override Try<L, B> ap<B>(Try<L, Function1<R, B>> ff) =>
      ff.bind((f) => map(f));

  // PURISTS BEWARE: side effecty stuff below -- proceed with caution!

  Iterable<R> toIterable() =>
      fold((_) => const Iterable.empty(), (r) => new _SingletonIterable(r));

  Iterator<R> iterator() => toIterable().iterator;

  void forEach(void sideEffect(R r)) => fold((_) => null, sideEffect);
}

class Failure<L extends Exception, R> extends Try<L, R> {
  final L _l;

  const Failure(this._l);

  L get value => _l;

  @override B fold<B>(B ifFailure(L l), B ifSuccess(R r)) => ifFailure(_l);

  @override bool operator ==(other) => other is Failure && other._l == _l;

  @override int get hashCode => _l.hashCode;

  @override
  B foldLeft<B>(B z, B Function(B previous, R a) f) {
    // TODO: implement foldLeft
    throw UnimplementedError();
  }

  @override
  B foldLeftWithIndex<B>(B z, B Function(B previous, int i, R a) f) {
    // TODO: implement foldLeftWithIndex
    throw UnimplementedError();
  }

  @override
  B foldRight<B>(B z, B Function(R a, B previous) f) {
    // TODO: implement foldRight
    throw UnimplementedError();
  }

  @override
  B foldRightWithIndex<B>(B z, B Function(int i, R a, B previous) f) {
    // TODO: implement foldRightWithIndex
    throw UnimplementedError();
  }
}

class Success<L extends Exception, R> extends Try<L, R> {
  final R _r;

  const Success(this._r);

  @override
  R get value => _r;

  @override B fold<B>(B ifFailure(L l), B ifSuccess(R r)) => ifSuccess(_r);

  @override bool operator ==(other) => other is Success && other._r == _r;

  @override int get hashCode => _r.hashCode;

  @override
  B foldLeft<B>(B z, B Function(B previous, R a) f) {
    // TODO: implement foldLeft
    throw UnimplementedError();
  }

  @override
  B foldLeftWithIndex<B>(B z, B Function(B previous, int i, R a) f) {
    // TODO: implement foldLeftWithIndex
    throw UnimplementedError();
  }

  @override
  B foldRight<B>(B z, B Function(R a, B previous) f) {
    // TODO: implement foldRight
    throw UnimplementedError();
  }

  @override
  B foldRightWithIndex<B>(B z, B Function(int i, R a, B previous) f) {
    // TODO: implement foldRightWithIndex
    throw UnimplementedError();
  }
}


Try<L, R> failure<L extends Exception, R>(L l) => new Failure(l);

Try<L, R> success<L extends Exception, R>(R r) => new Success(r);

class TryMonad<L extends Exception> extends MonadOpsMonad<Try<L, dynamic>> {
  TryMonad() : super(success);
}

final TryMonad TryM = new TryMonad();

TryMonad<L> tryM<L extends Exception>() => new TryMonad();
final Traversable<Try> TryTr = new TraversableOpsTraversable<Try>();

Traversable<Try<L, R>> tryTr<L extends Exception, R>() => new TraversableOpsTraversable();
/*
class TryTMonad<M> extends Functor<M> with Applicative<M>, Monad<M> {
  Monad _stackedM;
  TryTMonad(this._stackedM);
  Monad underlying() => TryM;

  @override M pure<A>(A a) => cast(_stackedM.pure(success(a)));
  @override M bind<A, B>(M mea, M f(A a)) => cast(_stackedM.bind(mea, (Try e) => e.fold((l) => _stackedM.pure(failure(l)), cast(f))));
}

Monad TryTMonad(Monad mmonad) => new TryTMonad(mmonad);
*/