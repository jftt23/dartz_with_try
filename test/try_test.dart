//import 'package:enumerators/enumerators.dart';
import 'enumerators_stubs.dart';
import 'package:test/test.dart';
//import 'package:enumerators/combinators.dart' as c;
import 'combinators_stubs.dart' as c;
import '../lib/dartz.dart';
//import 'dart:async';
import 'laws.dart';

class TranslationException implements Exception {
  dynamic e;
  TranslationException([this.e]);
}

class OddNumberException implements Exception{
  dynamic e;
  OddNumberException([this.e]);
}

void main() {

  test("demo", () {
    final IMap<int, String> intToEnglishMap = imap({1: "one", 2: "two", 3: "three"});
    final IMap<String, String> englishToSwedishMap = imap({"one": "ett", "two": "två"});

    Try<FormatException, int> stringToInt(String intString) => Try.catching<FormatException, int>(() => int.parse(intString)).failureMap((_) => FormatException("could not parse '$intString' to int"));
    Try<FormatException, String> intToEnglish(int i) => intToEnglishMap[i].toTry(() => FormatException("could not translate '$i' to english"));
    Try<FormatException, String> englishToSwedish(String english) => englishToSwedishMap[english].toTry(() => FormatException("could not translate '$english' to swedish"));
    Try<Exception, String> intStringToSwedish(String intString) => stringToInt(intString).bind(intToEnglish).bind(englishToSwedish);

    expect(intStringToSwedish("1"), success("ett"));
    expect(intStringToSwedish("2"), success("två"));
    expect(intStringToSwedish("fyrtiosjutton"), failure<FormatException, String>(FormatException("could not parse 'fyrtiosjutton' to int")));
    expect(intStringToSwedish("3"), failure<FormatException, String>(FormatException("could not translate 'three' to swedish")));
    expect(intStringToSwedish("4").isFailure(), true);
    expect(intStringToSwedish("4").isSuccess(), false);
    final t3 = intStringToSwedish("4");
    expect(t3.runtimeType, equals(Failure));
  });

  test("sequencing", () {
    final IList<Try<Exception, int>> l = ilist([success<Exception, int>(1), success<Exception, int>(2)]);
    expect(IList.sequenceTry(l), success<Exception,IList<int>>(ilist([1,2])));
    expect(Try.sequenceIList(IList.sequenceTry(l)), l);

    final IList<Try<Exception, int>> l2 = ilist([success<Exception, int>(1), failure<Exception, int>(Exception("out of ints...")), success<Exception, int>(2)]);
    expect(IList.sequenceTry(l2), failure<Exception, IList<int>>(Exception("out of ints...")));
    expect(Try.sequenceIList(IList.sequenceTry(l2)), ilist([failure<Exception, IList<int>>(Exception("out of ints..."))]));
  });

  group("TryM", () => checkMonadLaws(TryM));

  //group("TryTMonad+Id", () => checkMonadLaws(eitherTMonad(IdM)));

  //group("TryTMonad+IList", () => checkMonadLaws(eitherTMonad(IListMP)));

  group("TryM+Foldable", () => checkFoldableMonadLaws(TryTr, TryM));

  final Enumeration<Try<OddNumberException, int>> intTrys = c.ints.map((i) => i%2==0 ? success(i) : failure(OddNumberException(i)));

  group("TryTr", () => checkTraversableLaws(TryTr, intTrys));

  group("Try FoldableOps", () => checkFoldableOpsProperties(intTrys));

  test("iterable", () {
    expect(success(1).toIterable().toList(), [1]);
    expect(failure(Exception("nope")).toIterable().toList(), []);
  });

  group("Failure", () {
    test("value", () {
      final failure = Failure(FormatException("Wrong format"));
      expect(failure.value.runtimeType, FormatException);
      expect(failure.value.message, "Wrong format");
    });
  });

  group("Right", () {
    test("value", () {
      final right = new Success(2);
      expect(right.value, 2);
    });
  });
}
